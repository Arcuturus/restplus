import boto3 as b3
from pprint import pprint

# client = b3.client('iot')

class AWSApi:
    
    '''
    Simple class for connecting to AWS IoT core
    '''

    client = b3.client('iot')

    # works
    def list_things(self):
        return self.client.list_things()

    # works
    def describe_thing(self, name):
        return self.client.describe_thing(thingName=name)

    # testing
    def delete_thing(self, name):
        self.client.delete_thing(thingName=name)

    def create_thing(self, data):
        pprint(data)
        return self.client.create_thing(thingName=data['thingName'])
