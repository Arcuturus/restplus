from flask_restplus import Namespace, Resource, fields

from aws_api import AWSApi

api = Namespace('thing', description='Thing specific operations')
aws_iot = AWSApi()

key_value = api.model('key_value', {
    'string1': fields.String(require=False, desciption='key-value pair'),
    'string2': fields.String(require=False, desciption='key-value pair'),   
    'string3': fields.String(require=False, desciption='key-value pair')    
})

thing_attr = api.model('Attributes', {
    'attributes': fields.Nested(key_value, required=False,
        description='A JSON string containing up to three key-value pair in JSON format'),
    'merge': fields.Boolean(required=False, default=1, 
        description='Specifies whether the list of attributes provided in the AttributePayload is merged with the attributes stored in the registry, instead of overwriting them')
})

add_thing = api.model('Thing', {
    'thingName': fields.String(required=True, 
        description='The name of the thing to create', default=''),
    'thingTypeName': fields.String(required=False, 
        description='The name of the thing type associated with the new thing'),
    'attributePayload': fields.Nested(thing_attr, required=False, 
        description='The attribute payload, which consists of up to three name/value pairs in a JSON document'),
    'billingGroupName': fields.String(required=False,
        description='The name of the billing group the thing will be added to')
})

@api.route('/<string:thing_name>')
class Thing(Resource):
    '''Lets you view, add, edit and delete a specific Thing'''

    @api.doc('get_thing')
    def get(self, thing_name):
        '''Gets information about the specified thing.'''
        return aws_iot.describe_thing(thing_name)

    @api.doc('delete_thing')
    @api.response(204, 'Thing deleted')
    def delete(self, thing_name):
        '''Deletes the specified thing. Returns successfully with no error if the deletion is successful
        or you specify a thing that doesn't exist.'''
        return aws_iot.delete_thing(thing_name)


    @api.doc('update_thing')
    @api.expect(add_thing)
    @api.marshal_with(add_thing)
    def patch(self, thing_name):
        '''Updates the data for a thing.'''
        # client.update_thing(api.payload)
        return {'Status': 'updated'}


    @api.doc('add_thing')
    @api.expect(add_thing)
    @api.marshal_with(add_thing, code=201)
    def post(self, thing_name):
        # TODO: make this work
        '''Create new Thing'''
        return aws_iot.create_thing(api.payload)
        # return {'Status': 'created'}