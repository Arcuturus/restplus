from flask_restplus import Namespace, Resource, fields

from aws_api import AWSApi

api = Namespace('things', description='Things specific operations')
aws_iot = AWSApi()

@api.route('/things')
class Things(Resource):

    def get(self):
        '''Lists your things'''
        return aws_iot.list_things()